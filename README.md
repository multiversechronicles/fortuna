# RandomGenerator2

A system to dynamically generate objects with properties.


TODO: Place all this in the docs directory

Terms:
Object - What it sounds like
Property - Een "onderdeel" van een Object. Deze property heeft een waarde. Bijvoorbeeld een string.
PropertyType - Bepaald hoe de waarde van een Property gevuld wordt. Sommige propertyTypes hebben bepaalde Parameters nodig.
Template - Een PropertyType die een string als Parameter nodig heeft.
Parameter - Variables die een propertyType nodig heeft om de waarde van een Property te bepalen.

New syntax:
First make a list of choices that could be chosen from like so:

Family:
	choices:
	- Ralojor
	- Mirazana
	- Yinthyra
	- Mirafiel

Then you should add a "Choice" that determines which among the choices gets chosen. If you don't have a "Choice" it just picks one at random. But you can do a lot of neat stuff in "Choice" like so:

Conditional syntax (NOTE: start counting at 0, the first of the possible choices is 0)
	choice:
		expression: | 
			if (self.war == "Asgard"){
				return Random.choice ([0,3,6,9])
			} else if (self.war == "Utgard"){
				return Random.choice ([7,4,9])
			} else if (self.war == "Neutral"){
				return Random.choice ([5,4,3,7,8])
			}

Weighted syntax:
	choice:
		type: discrete_distribution
		probabilities:
		- 1
		- 4
		- 5
		- 1

To combine Conditional syntax and weights you can do this. Add 0 for the probabilities you don't want to choose:
    choice:
        expression: | 
            if (self.war == "Asgard"){
                return asgardValue
            } else if (self.war == "Utgard"){
                return utgardValue
            } else if (self.war == "Neutral"){
                return neutralValue
            }
        asgardValue:
            type: discrete_distribution
            probabilities:
            - 0
            - 0
            - 1
            - 1
        utgardValue:
            type: discrete_distribution
            probabilities:
            - 1
            - 1
            - 0
            - 0
        neutralValue:
            type: discrete_distribution
            probabilities:
            - 1
            - 1
            - 1
            - 1

If you want to include a list you already compiled inline in a sentence you can use the following in the choices list:
template: "You hate people that are {{ trait }}"
trait:
  include: Traits.yml
  
If you want to include a new short list inline in a sentence (and you are not planning on using the list somewhere else) you can use the following:
template: "You are attraced to people with {{ typeOfEyebrow }}"
typeOfEyebrow:
	choices:
	- fuzzy eyebrows
	- painted eyebrows
	- sentient eyebrows
  
		
If you want to use lists in the same file (but in multible places) you can use this YML trick:
.drinks: &drinks
	choices:
	- Beer
	- Wine

like:
	template: "drinking {{ drinks }}"
	drinks:
		<<: *drinks

dislike:
	template: "drinking {{ drinks }}"
	drinks:
		<<: *drinks