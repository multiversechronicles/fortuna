// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include <QVariant>
#include <QString>

#include <yaml-cpp/yaml.h>

namespace YAML {
    template <> struct convert<QVariant>
    {
        static Node encode(const QVariant& rhs)
        {
            return Node{rhs.toString().toStdString()};
        }

        static bool decode(const Node& node, QVariant& rhs)
        {
            switch(node.Type()) {
                case YAML::NodeType::Scalar: {
                    auto variant = QVariant{QString::fromStdString(node.Scalar())};

                    if (variant.convert(QMetaType::Double)) {
                        rhs = variant;
                    } else {
                        rhs = QVariant{QString::fromStdString(node.Scalar())};
                    }

                    break;
                }
                case YAML::NodeType::Null:
                    rhs = QVariant::fromValue(nullptr);
                    break;
                case YAML::NodeType::Undefined:
                    rhs = QVariant{};
                    break;
                case YAML::NodeType::Sequence:
                    rhs = node.as<QVariantList>();
                    break;
                case YAML::NodeType::Map:
                    rhs = node.as<QVariantMap>();
                    break;
                default:
                    return false;
            }
            return true;
        }
    };

    template <> struct convert<QString>
    {
        static Node encode(const QString& rhs)
        {
            return Node{rhs.toStdString()};
        }

        static bool decode(const Node& node, QString& rhs)
        {
            if (!node.IsScalar()) {
                return false;
            }

            rhs = QString::fromStdString(node.Scalar());
            return true;
        }
    };

    template <> struct convert<QVariantList>
    {
        static Node encode(const QVariantList& rhs)
        {
            auto node = Node{NodeType::Sequence};

            for (auto entry : rhs) {
                node.push_back(entry);
            }

            return node;
        }

        static bool decode(const Node& node, QVariantList& rhs)
        {
            if (!node.IsSequence()) {
                return false;
            }

            for (auto itr = node.begin(); itr != node.end(); ++itr) {
                rhs.append(itr->as<QVariant>());
            }
            return true;
        }
    };

    template <> struct convert<QVariantMap>
    {
        static Node encode(const QVariantMap& rhs)
        {
            auto node = Node{NodeType::Map};

            for (auto itr = rhs.begin(); itr != rhs.end(); ++itr) {
                node.force_insert(itr.key().toStdString(), itr.value());
            }

            return node;
        }

        static bool decode(const Node& node, QVariantMap& rhs)
        {
            if (!node.IsMap()) {
                return false;
            }

            for (auto itr = node.begin(); itr != node.end(); ++itr) {
                rhs.insert(itr->first.as<QString>(), itr->second.as<QVariant>());
            }
            return true;
        }
    };
}
