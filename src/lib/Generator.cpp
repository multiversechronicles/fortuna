// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "Generator.h"

#include <QDebug>

#include "GeneratedObject.h"
#include "Exceptions.h"
#include "Property.h"

#include "fortuna_logging.h"

using namespace Fortuna;

class FORTUNA_NO_EXPORT Generator::Private
{
public:
    QUrl file;
    std::unique_ptr<GeneratedObject> object = nullptr;
    QString propertyName;
    QString text;
    QString exceptionText;
};


Generator::Generator(QObject *parent)
    : QObject(parent)
    , d(std::make_unique<Private>())
{
}

Generator::~Generator() = default;

QUrl Generator::file() const
{
    return d->file;
}

void Generator::setFile(const QUrl& newFile)
{
    if (newFile == d->file) {
        return;
    }

    d->file = newFile;
    Q_EMIT fileChanged();
}

GeneratedObject* Generator::object() const
{
    return d->object.get();
}

QString Generator::propertyName() const
{
    return d->propertyName;
}

void Generator::setPropertyName(const QString & newPropertyName)
{
    if (newPropertyName == d->propertyName) {
        return;
    }

    d->propertyName = newPropertyName;

    if (d->object) {
        if (!d->propertyName.isEmpty() && d->object->hasProperty(d->propertyName)) {
            setText(d->object->value(d->propertyName).toString());
        } else  {
            setText(d->object->toString());
        }
    }

    Q_EMIT propertyNameChanged();
}

QString Generator::generatedText() const
{
    return d->text;
}

bool Generator::hasExceptions() const
{
    return !d->exceptionText.isEmpty();
}

QString Generator::exceptionText() const
{
    return d->exceptionText;
}

void Generator::generate()
{
    auto localFile = d->file.toLocalFile();

    if (localFile.isEmpty()) {
        setText(QString{});
        qCWarning(FORTUNA) << "Empty or invalid URL passed to Generator, cannot generate";
        return;
    }

    try {
        d->object = std::unique_ptr<GeneratedObject>(GeneratedObject::fromFile(localFile.toStdString()));
    } catch (GeneratorException& exception) {
        d->object = nullptr;
        setExceptionText(QString::fromUtf8(exception.what()));
        Q_EMIT objectChanged();
        return;
    }
    Q_EMIT objectChanged();

    d->object->generate();
    if (!d->propertyName.isEmpty() && d->object->hasProperty(d->propertyName)) {
        setText(d->object->value(d->propertyName).toString());
    } else  {
        setText(d->object->toString());
    }

    if (!d->object->exceptions().isEmpty()) {
        std::stringstream stream;
        const auto exceptions = d->object->exceptions();
        for (const auto& entry : exceptions) {
            stream << entry.what();
        }
        setExceptionText(QString::fromStdString(stream.str()));
    } else {
        setExceptionText(QString{});
    }
}

QString Generator::propertyText()
{
    return Property::listTypes();
}

void Generator::setText(const QString& text)
{
    if (text == d->text) {
        return;
    }

    d->text = text;
    Q_EMIT generatedTextChanged();
}

void Generator::setExceptionText(const QString& text)
{
    if (text == d->exceptionText) {
        return;
    }

    d->exceptionText = text;
    Q_EMIT exceptionTextChanged();
}
