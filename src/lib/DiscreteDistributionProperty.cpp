// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "DiscreteDistributionProperty.h"

#include <random>

#include <QDebug>

#include "RandomEngine.h"

using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(discrete_distribution, DiscreteDistributionProperty::createDiscreteDistributionProperty)

DiscreteDistributionProperty::DiscreteDistributionProperty(QObject* parent)
    : Property(parent)
{
}

DiscreteDistributionProperty::~DiscreteDistributionProperty()
{
}

void DiscreteDistributionProperty::generate()
{
    Property::generate();

    auto probabilityVariants = parameterValue<QVariantList>("probabilities"_qs).value_or(QVariantList{});

    auto probabilities = std::vector<qreal>{};
    std::transform(probabilityVariants.cbegin(), probabilityVariants.cend(), std::back_inserter(probabilities), [](const QVariant& value) {
        return value.toDouble();
    });

    auto distribution = std::discrete_distribution<int>(probabilities.cbegin(), probabilities.cend());

    setValue(distribution(RandomEngine::instance()->toStdRandomEngine()));
}

Property * DiscreteDistributionProperty::createDiscreteDistributionProperty(const YAML::Node& yaml)
{
    auto property = new DiscreteDistributionProperty();

    property->addRequiredParameter("probabilities"_qs);
    property->addParameters(yaml);

    return property;
}
