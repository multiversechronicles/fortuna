// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class DirectoryProperty : public Property
{
    Q_OBJECT
    Q_PROPERTY(QVariantList entries READ entries CONSTANT)
    Q_PROPERTY(QString directory READ directory CONSTANT)

public:
    DirectoryProperty(QObject* parent = nullptr);
    ~DirectoryProperty() override;

    QVariantList entries() const;
    QString directory() const;

    virtual void generate() override;

    static Property* create(const YAML::Node& yaml);
};

}
