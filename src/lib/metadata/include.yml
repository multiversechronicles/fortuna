id: include
name: "Include Another File"
weight: -10
category: Structural
description: |
    Include another file that defines a single property. The value from that
    property will be the value of the include.

    ### Parameters

    * **include** (required): The path to a file to include. The path should be
                              relative to the current object's file.
    * (any): All other parameters are used to override parameter values of the
             included property.

    ### Examples

    Include `SomeProperty.yml`, overriding its `name` parameter.

    ```yaml
    include_example:
        include: SomeProperty.yml
        name: "Some Property"
    ```

    SomeProperty.yml:

    ```yaml
    template: "Hello {{name}}"
    name: "World"
    ```

    ### Note

    When overriding a parameter of a property that matches the name of a
    property type, you must specify the include type explicitly by adding
    `type: include`, otherwise the property type detection becomes ambiguous.
