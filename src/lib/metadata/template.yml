id: template
name: "Text Template"
weight: -3
category: Generation
description: |
    Render a text template to a string.

    The template language is based on the Grantlee template language. You can
    use `{{ parameter }}` to output parameter values, though much more is
    possible, see http://www.grantlee.org/apidox/for_themers.html for details.

    The object currently being generated can be accessed through the special
    value `self`. Properties of that object can be accessed using
    `self.propertyName`. Only properties that have been declared before the
    template can be accessed.

    ### Parameters
    * **template** (required): The text template to render.
    * (any): All other parameters are passed to the template as values.

    ### Examples

    Generate either the string "Hello World" or "Hello Planet".

    ```yaml
    template_example:
        template: "Hello {{object}}"
        object:
            choices:
            - World
            - Planet
    ```

    Generate "The Weather is Good" or "The Weather is Bad", with Good and Bad
    read from a previously-declared property.

    ```yaml
    weather:
        choices:
        - Good
        - Bad

    template_example:
        template: "The Weather is {{ self.weather }}"
    ```
