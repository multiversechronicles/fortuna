// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class GeneratedObject;

class ObjectProperty : public Property
{
    Q_OBJECT
    Q_PROPERTY(Fortuna::GeneratedObject* object READ object CONSTANT)

public:
    ObjectProperty(QObject* parent = nullptr);
    ~ObjectProperty() override;

    GeneratedObject* object() const;

    virtual void generate() override;

    QString toString(int indentation = 0) const override;

    static Property* createObjectProperty(const YAML::Node& yaml);

private:
    GeneratedObject* m_object = nullptr;
};

}
