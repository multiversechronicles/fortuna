// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class NormalDistributionProperty : public Property
{
    Q_OBJECT

public:
    NormalDistributionProperty(QObject* parent = nullptr);
    ~NormalDistributionProperty();

    void generate() override;

    static Property* createNormalDistributionProperty(const YAML::Node& yaml);
};

}
