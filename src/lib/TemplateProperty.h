// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#pragma once

#include "Property.h"

namespace Fortuna {

class TemplateProperty : public Property
{
    Q_OBJECT
    Q_PROPERTY(QString templateString READ templateString CONSTANT)

public:
    TemplateProperty(QObject* parent = nullptr);
    ~TemplateProperty() override;

    void generate() override;

    QString templateString() const;

    QString toString(int indentation = 0) const override;

    static Property* createTemplateProperty(const YAML::Node& node);

private:
    class Private;
    const std::unique_ptr<Private> d;
};

}
