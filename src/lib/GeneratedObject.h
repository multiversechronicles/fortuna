
#pragma once

#include <memory>
#include <filesystem>

#include <QObject>
#include <QVector>
#include <QQmlListProperty>
#include <QQmlPropertyMap>

#include "fortuna_export.h"

namespace Fortuna {

class Property;
class GeneratorException;

class FORTUNA_EXPORT GeneratedObject : public QQmlPropertyMap
{
    Q_OBJECT

public:
    GeneratedObject(const QString& fileName = QString{}, QObject* parent = nullptr);
    ~GeneratedObject();

    Q_PROPERTY(QQmlListProperty<Fortuna::Property> properties READ propertiesList CONSTANT)
    QQmlListProperty<Property> propertiesList();
    QVector<Property*> properties() const;
    void setPropertyValue(const QString& propertyName, const QVariant& value);
    bool hasProperty(const QString& propertyName);

    Q_PROPERTY(QString fileName READ fileName CONSTANT)
    QString fileName() const;
    std::filesystem::path filePath() const;

    void generate();

    QVector<GeneratorException> exceptions() const;

    Q_INVOKABLE QString toString(int indentation = 0) const;

    static GeneratedObject* fromFile(const std::filesystem::path& path);

private:
    static int propertyCount(QQmlListProperty<Property>* list);
    static Property* propertyAt(QQmlListProperty<Property>* list, int index);

    class Private;
    const std::unique_ptr<Private> d;
};

}
