// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "QmlPlugin.h"

#include <QQmlEngine>
#include <QQuickItem>

#include "Generator.h"
#include "GeneratedObject.h"
#include "Property.h"
#include "RandomEngine.h"

using namespace Fortuna;

QmlPlugin::QmlPlugin(QObject *parent)
    : QQmlExtensionPlugin(parent)
{
}

void QmlPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(QString::fromLatin1(uri) == QLatin1String("Fortuna"));

    qmlRegisterRevision<QQuickItem, 15>(uri, 1, 0);

    qmlRegisterType<Generator>(uri, 1, 0, "Generator");
    qmlRegisterUncreatableType<GeneratedObject>(uri, 1, 0, "GeneratedObject", "Generated, use Generator to load a file"_qs);
    qmlRegisterUncreatableType<Property>(uri, 1, 0, "Property", "Generated, use Generator to load a file"_qs);

    qmlRegisterSingletonType<RandomEngine>(uri, 1, 0, "RandomEngine", [](QQmlEngine*, QJSEngine*) {
         auto engine = RandomEngine::instance().get();
         QQmlEngine::setObjectOwnership(engine, QQmlEngine::CppOwnership);
         return engine;
    });
}
