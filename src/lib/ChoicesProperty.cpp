// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "ChoicesProperty.h"

#include <QDebug>

#include "RandomEngine.h"

using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(choices, ChoicesProperty::create)

using namespace std::string_literals;

ChoicesProperty::ChoicesProperty(QObject* parent)
    : Property(parent)
{
}

ChoicesProperty::~ChoicesProperty()
{
}

QVariantList ChoicesProperty::choices() const
{
    if (childPropertyCount() > 1) {
        return childPropertyValues();
    } else {
        return childProperties().at(0)->childPropertyValues();
    }
}

void ChoicesProperty::generate()
{
    Property::generate();

    Property* childrenFrom = nullptr;
    if (!m_choices.IsSequence()) {
        auto property = generateProperty("choices"s, m_choices, this);
        addChildProperty(property);
        childrenFrom = property;
    } else {
        for (std::size_t i = 0; i < m_choices.size(); ++i) {
            auto property = generateProperty("choice_%1"_qs.arg(i).toStdString(), m_choices[i], this);
            addChildProperty(property);
        }
        childrenFrom = this;
    }

    if (m_choice.has_value()) {
        m_choiceProperty = generateProperty("choice"s, m_choice.value(), this);
        addChildProperty(m_choiceProperty);
    }

    auto choice = m_choiceProperty ? m_choiceProperty->value().toInt() : -1;

    if (choice == -1) {
        choice = RandomEngine::instance()->intValue(0, std::max(childrenFrom->childPropertyCount() - 1, 0));
    }

    if (choice < 0 || choice >= int(childrenFrom->childPropertyCount())) {
        throw PropertyException(path(), "Chosen index %1 is out of range"_qs.arg(choice));
    }

    m_chosenProperty = childrenFrom->childProperties().at(choice);
    setValue(m_chosenProperty->value());
}

Property *ChoicesProperty::chosen() const
{
    return m_chosenProperty;
}

Property* ChoicesProperty::create(const YAML::Node& yaml)
{
    auto property = new ChoicesProperty();

    auto choices = yaml["choices"];
    if (!choices) {
        throw GeneratorException("'choices' is a required parameter"_qs);
    }
    property->m_choices = choices;

    if (yaml["choice"]) {
        property->m_choice = yaml["choice"];
    }

    property->addParameters(yaml, {"choices"_qs, "choice"_qs});

    return property;
}
