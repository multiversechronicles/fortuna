// SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include "NormalDistributionProperty.h"

#include <random>

#include <QDebug>

#include "RandomEngine.h"

using namespace Fortuna;

FORTUNA_REGISTER_PROPERTY_TYPE(normal_distribution, NormalDistributionProperty::createNormalDistributionProperty)

NormalDistributionProperty::NormalDistributionProperty(QObject* parent)
    : Property(parent)
{
}

NormalDistributionProperty::~NormalDistributionProperty()
{
}

void NormalDistributionProperty::generate()
{
    Property::generate();

    auto mean = parameterValue<float>("mean"_qs).value_or(0.0);
    auto stddev = parameterValue<float>("stddev"_qs).value_or(1.0);

    auto min = parameterValue<float>("min"_qs).value_or(std::numeric_limits<float>::min());
    auto max = parameterValue<float>("max"_qs).value_or(std::numeric_limits<float>::max());

    auto distribution = std::normal_distribution<float>(mean, stddev);
    auto value = std::min(std::max(distribution(RandomEngine::instance()->toStdRandomEngine()), min), max);
    setValue(value);
}

Property * NormalDistributionProperty::createNormalDistributionProperty(const YAML::Node& yaml)
{
    auto property = new NormalDistributionProperty();

    property->addRequiredParameter("mean"_qs);
    property->addRequiredParameter("stddev"_qs);
    property->addParameters(yaml);

    return property;
}
