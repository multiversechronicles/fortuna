import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Particles 2.15

Item {
    id: root

    property var planet

    property var window: ApplicationWindow.window

    width: planet != null ? planet.distance * 2 * ApplicationWindow.window.auScalingFactor : 0
    height: width

    ParticleSystem {
        id: particles

        anchors.fill: parent
        Emitter {
            anchors.fill: parent
            shape: EllipseShape { fill: false }
            emitRate: 100
            lifeSpan: 10000
            lifeSpanVariation: 500
            size: 0.01 * ApplicationWindow.window.auScalingFactor
            sizeVariation: 0.005
        }

        ImageParticle {
            source: "qrc:/images/asteroid0.png"
            entryEffect: ImageParticle.None
            rotation: 1
            rotationVariation: 360
            rotationVelocity: 1
            rotationVelocityVariation: 10
        }

        ImageParticle {
            source: "qrc:/images/asteroid1.png"
            entryEffect: ImageParticle.None
            rotation: 1
            rotationVariation: 360
            rotationVelocity: 1
            rotationVelocityVariation: 10
        }

        Wander {
            affectedParameter: Wander.Velocity
            pace: 10
            xVariance: 0.01 * ApplicationWindow.window.auScalingFactor
            yVariance: 0.01 * ApplicationWindow.window.auScalingFactor
        }
    }

    Connections {
        target: root.window
        onAuScalingFactorChanged: particles.reset()
    }
}
