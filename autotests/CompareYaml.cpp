/*
 * SPDX-FileCopyrightText: 2022 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <QtTest>

#include "Property.h"
#include "GeneratedObject.h"

using namespace Fortuna;

class CompareYaml : public QObject
{
    Q_OBJECT

    void compareYaml(const YAML::Node& result, const YAML::Node& expected)
    {
        QCOMPARE(int(result.Type()), int(expected.Type()));
        QCOMPARE(result.size(), expected.size());

        if (expected.IsMap()) {
            QStringList resultKeys;
            QStringList expectedKeys;

            for (auto itr = expected.begin(); itr != expected.end(); ++itr) {
                expectedKeys.append(itr->first.as<QString>());
            }

            for (auto itr = result.begin(); itr != result.end(); ++itr) {
                resultKeys.append(itr->first.as<QString>());
            }

            QCOMPARE(resultKeys, expectedKeys);

            for (auto key : std::as_const(expectedKeys)) {
                compareYaml(result[key], expected[key]);
            }
        } else if (expected.IsSequence()) {
            for (std::size_t index = 0; index < expected.size(); ++index) {
                compareYaml(result[index], expected[index]);
            }
        } else {
            QCOMPARE(QString::fromStdString(result.Scalar()), QString::fromStdString(expected.Scalar()));
        }
    }

private Q_SLOTS:
    void test()
    {
        auto args = QCoreApplication::instance()->arguments();
        QVERIFY(args.size() == 3);

        auto source = args.at(1);
        auto expected = args.at(2);

        auto object = GeneratedObject::fromFile(std::filesystem::path(source.toStdString()));
        object->generate();

        if (!object->exceptions().isEmpty()) {
            const auto exceptions = object->exceptions();
            for (auto exception : exceptions) {
                QWARN(exception.what());
            }
            QFAIL("Exceptions were encountered processing the source file");
        }

        auto result = object->toString();

        auto resultYaml = YAML::Load(result.toStdString());
        auto expectedYaml = YAML::LoadFile(expected.toStdString());

        QVERIFY(resultYaml.IsMap());
        QVERIFY(expectedYaml.IsMap());

        compareYaml(resultYaml, expectedYaml);
    }
};

int main(int argc, char** argv)
{
    TESTLIB_SELFCOVERAGE_START(CompareYaml)
    QTest::Internal::callInitMain<CompareYaml>();

    QCoreApplication app(argc, argv);

    CompareYaml object;
    QTEST_SET_MAIN_SOURCE_PATH;
    return QTest::qExec(&object, QStringList{});
}

#include "CompareYaml.moc"
