
find_package(Qt5Test 5.15 CONFIG REQUIRED)

add_executable(compareyaml CompareYaml.cpp)
target_link_libraries(compareyaml Qt5::Test Fortuna)

macro(add_yaml_tests)
    foreach(test ${ARGV})
        add_test(NAME yaml-${test}
            COMMAND ${CMAKE_BINARY_DIR}/bin/compareyaml
                    "${CMAKE_CURRENT_SOURCE_DIR}/${test}/Test.yml"
                    "${CMAKE_CURRENT_SOURCE_DIR}/${test}/Expected.yml"
        )
    endforeach()
endmacro()

add_yaml_tests(
    "directory"
    "list"
    "expression"
)

ecm_add_tests(
    TestRandomEngine.cpp
    LINK_LIBRARIES Fortuna Qt5::Test
)
