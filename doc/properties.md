# Structural

## Include Another File (include)

Include another file that defines a single property. The value from that
property will be the value of the include.

### Parameters

* **include** (required): The path to a file to include. The path should be
                          relative to the current object's file.
* (any): All other parameters are used to override parameter values of the
         included property.

### Examples

Include `SomeProperty.yml`, overriding its `name` parameter.

```yaml
include_example:
    include: SomeProperty.yml
    name: "Some Property"
```

SomeProperty.yml:

```yaml
template: "Hello {{name}}"
name: "World"
```

### Note

When overriding a parameter of a property that matches the name of a
property type, you must specify the include type explicitly by adding
`type: include`, otherwise the property type detection becomes ambiguous.

## Object (object)

Create an instance of a specified object.

### Parameters

* **object** (required): A file path relative to the current file. It will
                         be used to create the new instance.
* (any): All other parameters will be used to initialize properties with the
         same name as the parameter on the object.

### Examples

Create an instance of "SomeObject.yml", overriding the name value to become
"Some Object".

```yaml
object_example:
    object: SomeObject.yml
    name: "Some Object"
```

`SomeObject.yml`:

```yaml
name: "Object Name"

other_property: Example
```

## Generate a List of Values (list)

Generate a list of values.

### Parameters

* **list** (required): A property description that will be used as a
                       template to generate a list value.
* **count** (required): The number of items to generate.

### Examples

Generate between 0 to 10 instances of "SomeObject.yml".

```yaml
list_example:
    list:
        object: SomeObject.yml
    count:
        type: random_integer
        from: 0
        to: 10
```

## JavaScript Expression (expression)

Generate a value by executing a JavaScript expression.

If the expression is a simple single-line string, such as `1 + 1`, you can
leave out `return`. If it is a more complex multi-line script, you should
make sure to `return` a value at the end of your script.

Parameters of the expression are passed to the script as variables. In
addition, there are a few special values:

* `self` points to the object currently being generated. You can access any
  property declared before this expression using `self.propertyName`.
* `parent` points to the parent of the expression. This can be chained to
  access the parent of the parent and so on.
* `Random` provides access to the random number engine. It exposes a number
  of functions for convenience:
  * `value(min, max)`: Generate a random floating point number between min
                       and max, inclusive.
  * `intValue(min, max)`: Generate a random integer number between min and
                          max, inclusive.
  * `realValue(min, max)`: An alias for `value()`.
  * `roll(count, sides, modifier)`: Simulate a dice roll. `count` number of
        dice with `sides` sides will be rolled, then `modifier` will be
        added.
  * `choice(list)`: Choose an item from `list` randomly. Each item has an
                    equal chance of being chosen.

Each property type exposes its parameters to the JavaScript environment, as
well as potentially other properties.

### Parameters

* **expression** (required): The expression to execute.
* (any): All other parameters will be available as variables in the
         expression.

### Examples

## List and Include the Contents of a Directory (directory)

List the contents of a directory and return that list. Unless `as_object` is
true, the list will contain an Include property for each file.

### Parameters

* **directory** (required): The path to a directory to list. The path should
                            be relative to the current object's file.
* **exclude**: A list of files to exclude.
* **as_object**: If true, include each file as an object instead of a
                 property.

# Generation

## Discrete Distribution (discrete_distribution)

Generates an integer value from a list of probabilities. The value will be
between 0 and the number of probabilities. The chance for a certain value to
be generated is the probability of that value divided by the sum of all
probabilities.

### Parameters

* **probabilities** (required): A list of probabilities.

### Examples

Generate a number between 0 and 5 with each successive number having a
higher chance of being generated.

The sum of probabilities is 15, so 0 has a chance of 1/15 to be generated,
while 5 has a chance of 5/15 or 1/3 to be generated.

```yaml
discrete_example:
    type: discrete_distribution
    probabilities:
    - 1
    - 2
    - 3
    - 4
    - 5
```

### Note

Since this type lacks a parameter that matches its type name, you will need
to explicitly indicate the type using `type: discrete_distribution`.

## Normal Distribution (normal_distribution)

Generates a floating-point value from a normal distribution, with the
specified mean and standard deviation.

### Parameters

* **mean** (required): The mean of the distribution.
* **stddev** (required): The standard deviation of the distribution.
* **min**: The minimum value that should be generated.
* **max**: The maximum value that should be generated.

### Examples

Generate a number between 0 and 5 from a normal distribution.

```yaml
normal_example:
    type: normal_distribution
    mean: 2.5
    stddev: 1
    min: 0
    max: 5
```

### Note

Since this type lacks a parameter that matches its type name, you will need
to explicitly indicate the type using `type: normal_distribution`.

## Random Floating-Point Value (random_float)

Generate a random floating point number between a start and end value,
inclusive.

### Parameters
* **from** (required): The start of the range.
* **to** (required): The end of the range.
* **modifier**: A fixed amount to add to the generated value.

### Examples

Generate a random value between 5 and 10.

```yaml
float_example:
    type: random_float
    from: 5
    to: 10
```

Generate a random value between 0 and 10 that is the sum of two random
values between 0 and 5.

```yaml
float_example:
    type: random_float
    from: 0
    to: 5
    modifier:
        type: random_float
        from: 0
        to: 5
```

### Note

Since this type lacks a parameter that matches its type name, you will need
to explicitly indicate the type using `type: random_float`.

## Random Integer Value (random_integer)

Generate a random integer between a start and end value, inclusive.

### Parameters

* **from** (required): The start of the range.
* **to** (required): The end of the range.
* **modifier**: A fixed amount to add to the generated value.

### Examples

Generate a random value between 5 and 10.

```yaml
integer_example:
    type: random_integer
    from: 5
    to: 10
```

### Note

Since this type lacks a parameter that matches its type name, you will need
to explicitly indicate the type using `type: random_float`.

## Text Template (template)

Render a text template to a string.

The template language is based on the Grantlee template language. You can
use `{{ parameter }}` to output parameter values, though much more is
possible, see http://www.grantlee.org/apidox/for_themers.html for details.

The object currently being generated can be accessed through the special
value `self`. Properties of that object can be accessed using
`self.propertyName`. Only properties that have been declared before the
template can be accessed.

### Parameters
* **template** (required): The text template to render.
* (any): All other parameters are passed to the template as values.

### Examples

Generate either the string "Hello World" or "Hello Planet".

```yaml
template_example:
    template: "Hello {{object}}"
    object:
        choices:
        - World
        - Planet
```

Generate "The Weather is Good" or "The Weather is Bad", with Good and Bad
read from a previously-declared property.

```yaml
weather:
    choices:
    - Good
    - Bad

template_example:
    template: "The Weather is {{ self.weather }}"
```

# Selection

## Choose From a List (choices)

Choose an item from a list of choices.

### Parameters

* **choices** (required): The list of items to choose from.
* **choice**: The index of the item to use. If not provided, an item will be
              chosen randomly, with each item having an equal chance of
              being chosen.

### Examples

Choose a random item from a list of items:

```yaml
choice_example:
    choices:
    - One
    - Two
    - Three
    - Four
    - Five
```

Choose an item from a list of items, based on a normal distribution.

```yaml
choice_example:
    choices:
    - One
    - Two
    - Three
    - Four
    - Five
    choice:
        type: normal_distribution
        mean: 3
        stddev: 1
```

### Note

The value of `choice` should be an integer. If the value is not an integer,
it will be converted to one. This has some potential side effects. Most
importantly, floating point values will be converted by discarding the parts
after the decimal period (flooring).

## Select a Parameter (select)

Select a value from the provided parameters based on a value.

### Parameters

* **select** (required): The value to use for the selection.
* **match_case**: If true (the default), match parameter names
                  case-sensitive, otherwise ignore casing.
* (any): All other parameters are used as options to select from. The name
         of the parameter should match the generated value.

### Examples

This will output a different string depending on which value was selected
from a list of values.

```yaml
select_example:
    select:
        choices:
        - zero
        - one
        - two
    zero: "Zero was generated"
    one: "One was generated"
    two: "Two was generated"
```

# Other

## Literal Value (literal)

Any value that is not a map will be converted to a literal property. This
means that you can specify any constant value by providing it as a simple
value. In addition, any map that does not have a type set and does not match
any of the known property types will also be converted to a literal
property.

The literal type can also be specified explicitly, which means that the map
will be converted to a literal property, even if it would normally match one
of the known property types.

