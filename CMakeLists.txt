cmake_minimum_required(VERSION 3.16)

project(Fortuna VERSION 2022.01.00)

find_package(ECM 5.87 CONFIG REQUIRED)
list(APPEND CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

find_package(Qt5 REQUIRED Quick Widgets)
find_package(yaml-cpp REQUIRED)
find_package(Grantlee5 REQUIRED)

include(ECMGenerateExportHeader)
include(ECMQmlModule)
include(ECMAddTests)
include(ECMQtDeclareLoggingCategory)
include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings)

kde_enable_exceptions()

if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
    set(BINARY_INSTALL_DIR ".")
else()
    set(BINARY_INSTALL_DIR ${KDE_INSTALL_BINDIR})
endif()

add_subdirectory(src/lib)
add_subdirectory(src/commandline)
add_subdirectory(src/editor)
add_subdirectory(src/starsystemui)

if (BUILD_TESTING)
    add_subdirectory(autotests)
endif()

set(CPACK_GENERATOR ZIP)
set(CPACK_INSTALLED_DIRECTORIES "${CMAKE_SOURCE_DIR}/data;/data")
set(CPACK_INSTALL_SCRIPTS "${CMAKE_SOURCE_DIR}/Package.cmake")
include(CPack)
